const electron=require('electron'),handler=require(__dirname+'/handler.js'),
	Host='http://ditrox.foroactivo.com/',
	fs = require('fs'),
	request = require('request');
let mainWindow;

var storeSignedRequest=[];

var  HackTool={
	data:{},
	index:0,
	count:0,
	hacklist:[],
	sites:{
		"Dragon City":[
				"http://vina-full.com/dragon-city-hack-tool.html",
				"http://dc4u.eu/dragon-city-hack-tool.html",
				"http://dc4in.com/dragon-city/"
		],
		"Monster Legends":[
				"http://dc4u.eu/monster-legends-hack-tool.html",
				"http://vina-full.com/monster-legends-hack-tool.html",
				"http://dc4in.com/monster-legend/"
		],
		"Candy Crush Saga":[
					'http://dc4u.eu/candy-crush-saga-hack-tool.html',
					'http://vina-full.com/candy-crush-saga-hack-tool.html',
					'http://dc4in.com/candy-crush-saga/'
		],
		"Candy Crush Soda Saga":[
					'http://dc4u.eu/candy-crush-soda-hack-tool.html',
					'http://vina-full.com/candy-crush-soda-hack-tool.html'
		],
		"Farm Heroes Saga":[
					'http://dc4u.eu/farm-heroes-saga-hack-tool.html',
					'http://vina-full.com/farm-heroes-saga-hack-tool.html'
		],
		"Farm Heroes Super Saga":[
					'http://dc4u.eu/farm-heroes-super-saga-hack-tool.html',
					'http://vina-full.com/farm-heroes-super-saga-hack-tool.html',
					'http://dc4in.com/farm-heroes-saga/'
		],
		"Diamond Digger Saga":[
					'http://dc4u.eu/diamond-digger-saga-hack-tool.html',
					'http://vina-full.com/diamond-digger-saga-hack-tool.html',
					'http://dc4in.com/diamond-digger-saga/'
		],
		"Papa Pear Saga":[
					'http://dc4u.eu/papa-pear-saga-hack-tool.html',
					'http://vina-full.com/papa-pear-saga-hack-tool.html'
		],
		"Bubble Witch Saga":[
					'http://dc4u.eu/bubble-witch-saga-hack-tool.html',
					'http://vina-full.com/bubble-witch-saga-hack-tool.html'
		],
		"Bubble Witch 2 Saga":[
					'http://dc4u.eu/bubble-witch-2-saga-hack-tool.html',
					'http://vina-full.com/bubble-witch-2-saga-hack-tool.html'
		],
		"Pepper Panic Saga":[
					'http://dc4u.eu/pepper-panic-saga-hack-tool.html',
					'http://vina-full.com/pepper-panic-saga-hack-tool.html'
		],
		"Alphabetty Saga":[
					'http://dc4u.eu/alphabetty-saga-hack-tool.html',
					'http://vina-full.com/alphabetty-saga-hack-tool.html'
		],
		"Criminal Case":[
					'http://dc4u.eu/criminal-case-hack-tool.html',
					'http://vina-full.com/criminal-case-hack-tool.html'
		],
		"Bubble Coco":[
					'http://dc4u.eu/bubble-coco-hack-tool.html',
					'http://vina-full.com/bubble-coco-hack-tool.html'
		],
		"Buggle":[
					'http://dc4u.eu/buggle-hack-tool.html',
					'http://vina-full.com/buggle-hack-tool.html'
		],
		"Pengle":[
					'http://dc4u.eu/pengle-hack-tool.html',
					'http://vina-full.com/pengle-hack-tool.html'
		],
		"Pig & Dragon":[
					'http://dc4u.eu/pig-dragon-hack-tool.html',
					'http://vina-full.com/pig-dragon-hack-tool.html'
		],
		"Color Pop":[
					'http://dc4u.eu/color-pop-hack-tool.html',
					'http://vina-full.com/color-pop-hack-tool.html'
		],
		"Pudding Pop":[
					'http://dc4u.eu/pudding-pop-hack-tool.html',
					'http://vina-full.com/pudding-pop-hack-tool.html'
		],
		"Tasty Pop":[
					'http://dc4u.eu/tasty-pop-hack-tool.html',
					'http://vina-full.com/tasty-pop-hack-tool.html'
		],
		"Solitaire in Wonderland":[
					'http://dc4u.eu/solitaire-in-wonderland-hack-tool.html',
					'http://vina-full.com/solitaire-in-wonderland-hack-tool.html'
		]
	}
}

function createWindow(){
	mainWindow = new electron.BrowserWindow(
		{
			title:"DitroX AHT",
			icon:__dirname+'/64.jpg',
			width: 940,
			height: 640,
			resizable:false,
			frame:false,
			webPreferences: {
				plugins: true,
				preload:__dirname+'/data/panel/script.js'
			}
		}
	)
	electron.session.fromPartition('sessionHackTool').webRequest.onBeforeSendHeaders({urls:['*']},function(details, callback){
		var cancelthis=false;
		if(!details.url.includes("jquery")&&details.url.match(/\.(jpeg|jpg|gif|png|ico|css|js)$/) != null){
			cancelthis=true;
		}
		if(details.url.includes("vina-full")){
			details.requestHeaders["Referer"]="http://linkshrink.net/7DORDi"
		} else if(details.url.includes("dc4u.eu")){
			details.requestHeaders["Referer"]="http://linkshrink.net/7TJTKW"
		} else if(details.url.includes("dc4in")){
			cancelthis=false;
		}
		callback({cancel: cancelthis, requestHeaders: details.requestHeaders})
	})
	mainWindow.loadURL(Host+"/h2-");

	//Menu task returns
	electron.ipcMain.on('taskAction',function(event,n){
		switch(n){
			case 0:
				mainWindow.minimize();
				break;
			case 1:
				mainWindow.close();
				break;
		}
	})



	electron.ipcMain.on('askNewSite',function(event){
		if(HackTool.index>=HackTool.hacklist.length){
			HackTool.index=0;
		}
		event.sender.send('returnNewSite',HackTool.hacklist[HackTool.index]);
		HackTool.index+=1;
	});

	electron.ipcMain.on('requestData',function(event){
		event.sender.send('data',HackTool);
	});

	electron.ipcMain.on('toZero',function(event){
		HackTool.count=0;
	});

	electron.ipcMain.on('startHT',function(event,dataS){
		HackTool.data=dataS;
		HackTool.hacklist=HackTool.sites[HackTool.data.game];
		var count=0;
		electron.ipcMain.on('result',function(e,info){
			count++;
			HackTool.count+=1;
			event.sender.send('showResult',info,count);
		})
	})

	electron.ipcMain.on('storeSignedRequest',function(event,data,url){
		storeSignedRequest=[data,url];
	})
	electron.ipcMain.on('askSignedRequest',function(event){
		event.sender.send('returnSignedRequest',storeSignedRequest);
	})

var eventSender;
var mainEvent;
	electron.ipcMain.on('newMessage',function(ese,m){
		eventSender.sender.send('sendMessage',m);
	});
	electron.ipcMain.on('waitLogin',function(event){
		mainEvent=event;
	})
	electron.ipcMain.on('facebook',function(eventC1){
		mainEvent.sender.send("logged")
	})
	electron.ipcMain.on('storeIds',function(e,ids){
		mainEvent.sender.send('returnIds',ids);
	})
	electron.ipcMain.on('sendChat',function(e,a){
		mainEvent.sender.send('showChat',a);
		eventSender=e;
	})
	mainWindow.on('closed', function () {
		mainWindow = null
	})
}

//Main functions
electron.app.on('ready', function(){
	if(handler.initUpdate(electron)){
		createWindow();
	}
})
electron.app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    electron.app.quit()
  }
})
electron.app.on('activate', function () {
  if (mainWindow === null) {
	if(handler.initUpdate(electron)){
		createWindow();
	}
  }
})
