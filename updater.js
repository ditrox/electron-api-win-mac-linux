const {ipcRenderer} = require('electron');
document.onreadystatechange = function () {
	if (document.readyState == "complete") {
		document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });
		ipcRenderer.send("updateLink",document.querySelector("#uploaded-files a").href);
	}
}