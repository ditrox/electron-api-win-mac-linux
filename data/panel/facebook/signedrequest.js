const {ipcRenderer} = require('electron');
document.onreadystatechange = function () {
	if (document.readyState == "complete") {
		if(document.domain.includes("facebook")){
			ipcRenderer.send("storeSignedRequest",document.getElementsByName("signed_request")[0].value,document.URL);
			window.location.href="http://www.ha-lab.com/";
		} else {
			ipcRenderer.send("askSignedRequest");
		}
	}
}
ipcRenderer.on("returnSignedRequest",function(e,data){
	var game="";
	var parseURL=data[1].split("/")[3];
	switch (parseURL){
		case "dragoncity":
			game="sp_dc";
			break;
		case "monsterlegends":
			game="sp_ml";
			break;
		case "candycrush":
			game="kg_cc";
			break;
		case "candycrushsoda":
			game="";
			break;
		case "farmheroes":
			game="kg_fh";
			break;
		case "farmheroessupersaga":
			game="";
			break;
		case "diamonddiggersaga":
			game="kg_dd";
			break;
		case "papapear":
			game="kg_pp";
			break;
		case "bubblewitch":
			game="kg_bw";
			break;
		case "bubblewitch-two":
			game="kg_bw2";
			break;
		case "pepperpanicsaga":
			game="kg_pe";
			break;
		case "alphabettysaga":
			game="";
			break;
		case "criminalcase":
			game="ps_cc";
			break;
		case "bubblecoco":
			game="ca_bc";
			break;
		case "playbuggle":
			game="ca_bu";
			break;
		case "playpengle":
			game="ca_pe";
			break;
		case "playpigdragon":
			game="ca_pd";
			break;
		case "playcolorpop":
			game="ca_cp";
			break;
		case "playpudding":
			game="ca_pp";
			break;
		case "playtastypop":
			game="ca_tp";
			break;
		case "solitaireinwonder":
			game="ca_so";
			break;
	}
	var http = new XMLHttpRequest();
	var url = "/assets/php/Actions/GetInfo.php";
	var params = "sign="+data[0]+"&game="+game;
	http.open("POST", url, true);

	//Send the proper header information along with the request
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

	http.onreadystatechange = function() {//Call a function when the state changes.
			if(http.readyState == 4 && http.status == 200) {
					ipcRenderer.send("storeIds",http.responseText);
			}
	}
	http.send(params);
});
