const {ipcRenderer} = require('electron');
function sendMessage(m){
	document.querySelector("#composerInput").value=m;
  var keyboardEvent = document.createEvent("KeyboardEvent");
  var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? "initKeyboardEvent" : "initKeyEvent";
  keyboardEvent[initMethod](
                     "keydown", // event type : keydown, keyup, keypress
                      true, // bubbles
                      true, // cancelable
                      window, // viewArg: should be window
                      false, // ctrlKeyArg
                      false, // altKeyArg
                      false, // shiftKeyArg
                      false, // metaKeyArg
                      40, // keyCodeArg : unsigned long the virtual key code, else 0
                      0 // charCodeArgs : unsigned long the Unicode character associated with the depressed key, else 0
  );
  document.querySelector("#composerInput").dispatchEvent(keyboardEvent);
  document.querySelector("button[value='Send']").click();
}
document.onreadystatechange = function () {
	if (document.readyState == "complete") {
		ipcRenderer.send("waitingMessage");
		sendMessage("$New user$");
    setInterval(function(){
      ipcRenderer.send("sendChat",document.querySelector("#messageGroup").innerHTML.replace(/ *\$[^$]*\$ */g, " "));
    },1000);
  }
}
ipcRenderer.on("sendMessage",function(e,m){
  sendMessage(m);
});
