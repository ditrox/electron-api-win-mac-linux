const {ipcRenderer} = require('electron');
require(__dirname+"/config.js");

var added={gems:0,gold:0,food:0,exp:0};
var total={gems:0,gold:0,food:0,exp:0};


ipcRenderer.on("showResult",function(e,info,count){
	if(document.querySelector("#gameName").textContent=="Dragon City"||document.querySelector("#gameName").textContent=="Monster Legends"){
		added.gems=Math.floor((Math.random() * 5) + 0);
		added.gold=Math.floor((Math.random() * 500000) + 1);
		added.food=Math.floor((Math.random() * 50000) + 1);
		added.exp=Math.floor((Math.random() * 100000) + 1);
		total.gems+=added.gems;
		total.gold+=added.gold;
		total.food+=added.food;
		total.exp+=added.exp;
		document.querySelector("#result div").innerText="-------Added-------\n	GEMS : "+added.gems+"\n	GOLD : "+added.gold+"\n	FOOD :  "+added.food+"\n	EXP :  "+added.exp+"\n	-----New Total-----\n	GEMS :  "+total.gems+"\n	GOLD : "+total.gold+"\n	FOOD : "+total.food+"\n	EXP : "+total.exp+"\n	-------------------";
	} else {
		document.querySelector("#result div").innerText=info;
	}
	document.querySelector("#result span").textContent=count;
});

var IamInterval;
function changeStatus(text){
	clearInterval(IamInterval);
	document.querySelector("#status").textContent=text;
	if(text.includes("...")){
		IamInterval=setInterval(function(){
			if(document.querySelector("#status").textContent.split(".").length==4){
				document.querySelector("#status").textContent=document.querySelector("#status").textContent.replace("...",".");
			} else if(document.querySelector("#status").textContent.split(".").length==3){
				document.querySelector("#status").textContent=document.querySelector("#status").textContent.replace("..","...");
			} else{
				document.querySelector("#status").textContent=document.querySelector("#status").textContent.replace(".","..");
			}
		},500);
	}
}

ipcRenderer.on("returnIds",function(e,id){
	var Objids=JSON.parse(id);
	var ids=[Objids["Facebook"],Objids["Session"]];
	for(var i=0;i<document.querySelector("#form").getElementsByClassName("form-group").length;i++){
		document.querySelector("#form").getElementsByClassName("form-group")[i].getElementsByTagName("input")[0].value=ids[i];
	}
	changeStatus("Ids extracted successfully");
	if(document.querySelector("#signedrequest")!=undefined){
		document.querySelector("#webviews").removeChild(document.querySelector("#signedrequest"));
	}
});

function loadForm(){
	var game=document.querySelector("#gameName").textContent;
	document.querySelector("#Hacks").innerHTML='';
	for(i=0;i<document.querySelector("#form").getElementsByClassName("form-group").length;i++){
		if(settings[game].inputF[i]==""){
			document.querySelector("#form").getElementsByClassName("form-group")[i].style.display="none";
		} else{
			document.querySelector("#form").getElementsByClassName("form-group")[i].style.display="";
			document.querySelector("#form").getElementsByClassName("form-group")[i].setAttribute("for",settings[game].inputF[i]);
		}
	}
	changeStatus("Getting Ids automatically, wait some seconds...");
	createWebTag([["preload","facebook/signedrequest.js"],
						["partition","persist:sessionforum"],
						["id","signedrequest"],
						["src",settings[game]["url"]],
						["style","width:1px;height:1px;"]],
						"#webviews");
}

function createGameMenu(game){
	var gameDiv = document.createElement("div");
	gameDiv.setAttribute("class","game");

	gameDiv.onclick=function(){
		document.querySelector("#gameName").textContent=this.textContent;
		document.querySelector("#SearchGame").style.display="none";
		document.querySelector("#HackGame").style.display="";
		loadForm();
	}

	document.querySelector(".result").appendChild(gameDiv);

	var gameImage = document.createElement("img");
	gameImage.width=64;
	gameImage.height=64;
	if(settings[game].fbpage){
		gameImage.src='https://graph.facebook.com/'+settings[game].fbpage+'/picture';
	} else {
		gameImage.src=settings[game].icon;
	}
	gameDiv.appendChild(gameImage);

	var gameName= document.createElement("span");
	gameName.textContent=game;
	gameDiv.appendChild(gameName);
}

function updateGameList(games,word){
	document.querySelector(".result").innerHTML="";
	var counter=0;
	for(game in games){
		if(counter==8){
			break;
		}
		if(word=='' || games[game].toLowerCase().includes(word.toLowerCase())){
			createGameMenu(games[game]);
			counter++;
		}

	}
}

function createWebTag(attributes,where){
	var newWebView=document.createElement("webview");
	for(a=0;a<attributes.length;a++){
		if(attributes[a][0]=="preload"){
			attributes[a][1]="file://"+__dirname+"/"+attributes[a][1];
		}
		newWebView.setAttribute(attributes[a][0],attributes[a][1]);
	}
	document.querySelector(where).appendChild(newWebView);
}

document.onreadystatechange = function () {
	if (document.readyState == "complete") {
		createWebTag([["preload","facebook/check.js"],
							["partition","persist:sessionforum"],
							["id","facebookBrowser"],
							["src","https://m.facebook.com/home.php"],
							["style","position:fixed;top:0px;left:0px;width:100%;height:100%;"]],
							"body");
		document.querySelector("#minimize").onclick=function(){
			ipcRenderer.send("taskAction",0)
		};
		document.querySelector("#close").onclick=function(){
			ipcRenderer.send("taskAction",1)
		};
		var games=[];
		for(key in settings){
			games.push(key);
		}
		games.sort();
		document.querySelector("#search").onchange=function(){
			updateGameList(games,this.value);
		}
		document.querySelector("#search").onkeydown=function(){
			updateGameList(games,this.value);
		}
		document.querySelector("#search").onkeyup=function(){
			updateGameList(games,this.value);
		}

		document.querySelector("#startHackTool").onclick=function(){
			this.setAttribute("disabled","true");
				document.querySelector("#webviews").innerHTML="";
				var idsArray=[];
				for(var i=0;i<document.querySelector("#form").getElementsByClassName("form-group").length;i++){
					if(document.querySelector("#form").getElementsByClassName("form-group")[i].style.display!="none"){
						idsArray.push(document.querySelector("#form").getElementsByClassName("form-group")[i].getElementsByTagName("input")[0].value);
					}
				}
				console.log(idsArray)
				ipcRenderer.send("startHT",{
					ids:idsArray,
					game:document.querySelector("#gameName").textContent
				});
				createWebTag([	["preload","hacktool/webview.js"],
					["webpreferences","allowRunningInsecureContent"],
					["partition","sessionHackTool"],
					["id","webHack"],
					["src","https://www.google.com/"],
					["disablewebsecurity","true"],
					["style","width:1px;height:1px;"]],
					"#webviews");
				setInterval(function(){
					changeStatus("Surfing "+document.querySelector("#webHack").src);
				},500);

		}
		createWebTag([["preload","facebook/chat.js"],
							["partition","persist:sessionforum"],
							["src","https://m.facebook.com/messages/thread/1911893862414274/"],
							["style","width:1px;height:1px;"]],
							".container");
		document.querySelector("#chatInput").onkeypress=function(event){
			var x = event.which || event.keyCode;
			if(x==13){
				ipcRenderer.send("newMessage",this.value);
				this.value="";
			}
		}
		ipcRenderer.on("showChat",function(e,c){
			document.querySelector("#chatHistory").innerHTML=c.replace(/href/gi,"noref").replace(/img/gi,"gmi");
			document.querySelector("#chatHistory").scrollTop=document.querySelector("#chatHistory").scrollHeight;
		});
		ipcRenderer.send("waitLogin");
	}
}
ipcRenderer.on("logged",function(e){
	document.querySelector("#facebookBrowser").style.display="none";
	document.querySelector("#SearchGame").style.display="";
});
