const unzip=require('unzip'),
	request = require('request'),
	fs=require('fs');
// =======================
	//Debugg mode
// =======================

	var debugg=false;

// =======================

// =======================
let updateWindow;

function getConfigF(){
	var blankConfig={
		"update":"",
		"tos":""
	}
	try{
		if(fs.existsSync(__dirname+'\\config.json')){
			var getConfig=fs.readFileSync(__dirname+'\\config.json');
			config=JSON.parse(getConfig.toString());
		} else {
			fs.writeFile(__dirname+'\\config.json',JSON.stringify(blankConfig), function(err) {});
			config=blankConfig
		}
		return config
	}catch(e){
		return blankConfig;
	}
}

exports.getDomain=function(url){
	urlParts = /^(?:\w+\:\/\/)?([^\/]+)(.*)$/.exec(url);
	hostname = urlParts[1];
	return hostname.replace("www.","").split(".")[0];
}
var electron;

function updateEverything(e,link){
	var config=getConfigF();
	if(link!=config["update"]){
		dir="ditrox-electron-api-win-mac-linux-"+link.replace("https://bitbucket.org/ditrox/electron-api-win-mac-linux/get/","").replace(".zip","");
		request(link)
		.pipe(fs.createWriteStream(__dirname+'\\DXAHT.zip'))
		.on('close', function () {
			fs.createReadStream(__dirname+'\\DXAHT.zip')
			.pipe(unzip.Parse())
			.on('entry', function (entry) {
				if(entry.type === "Directory" && entry.path!==dir+"/"){
					if (!fs.existsSync(__dirname+'/'+entry.path.replace(dir,""))){
						fs.mkdirSync(__dirname+'/'+entry.path.replace(dir,""));
					}
				} else if (entry.type !== "Directory") {
					entry.pipe(fs.createWriteStream(__dirname+'\\'+entry.path.replace(dir,"")));
				}
			})
			.on('close', function () {
				fs.unlinkSync(__dirname+'\\DXAHT.zip');
				config["update"]=link;
				fs.writeFile(__dirname+'\\config.json',JSON.stringify(config), function(err) {});
				electron.app.relaunch({args: process.argv.slice(1).concat(['--relaunch'])})
				electron.app.exit(0);
				return false;
			});
		});
	} else {
		electron.session.defaultSession.clearCache(function(){
			if(updateWindow!=null){
				updateWindow.close();
				updateWindow=null;
			}
		});
		return true;
		
	}
}
exports.initUpdate = function(elec){
	try{
		if(debugg){
			return true;
		} else{
			electron=elec;
			updateWindow = new electron.BrowserWindow({
				show:false,
				webPreferences:{
					preload:__dirname+'\\updater.js'
				}
			})
			updateWindow.loadURL("https://bitbucket.org/ditrox/electron-api-win-mac-linux/downloads/");
			return electron.ipcMain.on("updateLink",updateEverything);
		}
	}catch(e){
		electron.session.defaultSession.clearCache(function(){
			if(updateWindow!=null){
				updateWindow.close();
				updateWindow=null;
			}
		});
		return true;
	}
}

exports.getConfig = function(){
	return getConfigF();
}

exports.saveConfig=function(config){
	fs.writeFile(__dirname+'\\config.json',JSON.stringify(config), function(err) {});
}